
### 开始使用
1. #### npm install 
[https://www.npmjs.com/package/echarts-panel](https://www.npmjs.com/package/echarts-panel)

`npm i echarts-panel`


1. #### 导入组件

`import {
    VueEchart,
    CircularEyeProgress,
    CircularProgress,
    PictorialProgress,
    SpectrumProgress,
    WarningProgress,
    LiquidChart
} from "VueEchart";`


2. #### CircularEyeProgress
![](./example/CircularEyeProgress.png)

#### 配置颜色 代码示例：
`<CircularEyeProgress
foreground-color="#0094ff"
style="width: 450px;height: 450px;"
/>` 

#### 效果如下：

![](./example/CircularEyeProgressBlue.png)

3. #### PictorialProgress
![](./example/PictorialProgress.png)


4. #### CircularProgress
![](./example/CircularProgress.png)


5. #### SpectrumProgress
![](./example/SpectrumProgress.png)


6. #### WarningProgress
![](./example/WarningProgress.png)



7. #### LiquidChart
![](./example/LiquidChart.png)
